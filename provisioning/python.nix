{ pkgs ? import ../nix { }, debug ? false }:
let
  python = pkgs.python310.override {
    packageOverrides = self: super: {
      django = super.django_4;
      authens = self.callPackage ./authens.nix { };
      pythoncas = self.callPackage ./python-cas.nix { };
    };
  };
in
python.withPackages (ps: [
  ps.django
  ps.djangorestframework
  ps.authens
] ++ pkgs.lib.optionals debug [
  ps.django-debug-toolbar
  ps.black
  ps.isort
])
