{ lib, pythoncas, django, ldap, buildPythonPackage }:
buildPythonPackage rec {
  pname = "authens";
  version = "v0.1b5";
  doCheck = false;
  src = builtins.fetchGit {
    url = "https://git.eleves.ens.fr/klub-dev-ens/authens.git";
    #rev = "master";
    #sha256 = "sha256-R0Nw212/BOPHfpspT5wzxtji1vxZ/JOuwr00naklWE8=";
  };
  propagatedBuildInputs = [ django ldap pythoncas ];
}
