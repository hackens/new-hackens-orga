{ pkgs, settings, source, app }:
let
  manage-py-file = "${source}/${app}/manage.py";
  static-assets = pkgs.callPackage ./static-assets.nix { inherit python managePy; envPrefix = "HACKENS_ORGA_"};
  mkEnv = settings: let # make env file to source before using manage.py and other commands
    lib = pkgs.lib;
    mkVarVal = v: let
      isHasAttr = s: isAttrs v && hasAttr s v;
    in
    if builtins.isString v then lib.escapeShellArg v
    # NOTE: If any value contains a , (comma) this will not get escaped
    else if builtins.isList v && any lib.strings.isCoercibleToString v then lib.escapeShellArg (concatMapStringsSep "," toString v)
    else if builtins.isInt v then toString v
    else if builtins.isBool v then toString (if v then 1 else 0)
    else if isHasAttr "_file" then "$(cat ${lib.escapeShellArg v._file} | xargs)"
    else if isHasAttr "_raw" then v._raw
    else abort "The django conf value ${lib.generators.toPretty {} v} can not be encoded.";
  in lib.concatStrinsSep "\n" (lib.mapAttrsToList (k: v: "export ${k}=${mkVarVal v}") settings);
  envFile = mkEnv settings;
  managePy = pkgs.writeScript "manage-${app}" ''
    source ${envFile}
    ${python}/bin/python ${manage-py-file} $@
  '';
{
  inherit managePy static-assets envFile source;
}
