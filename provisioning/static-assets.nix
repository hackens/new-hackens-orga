{ python, managePy, envPrefix ? ""}:
pkgs.runCommand "${name}-static" { buildInputs = [ src python ]; } ''
  mkdir $out
  export ${envPrefix}SECRET_KEY="collectstatic"
  export ${envPrefix}STATIC_ROOT=$out
  export ${envPrefix}DEBUG=0
  export ${envPrefix}ALLOWED_HOSTS=
  export ${envPrefix}DB_FILE=
  ${managePy} collectstatic
''
