{ pkgs ? import ../nix { } }:
pkgs.mkShell {
  buildInputs = [
    (import ./python.nix { inherit pkgs; debug = true; })
  ];
}
