{ pkgs, lib, config }:
let
  app = "hackens-orga";
  cfg = config.services.django.${app};
  assets = import ./mk-assets.nix {
    inherit pkgs app;
    settings = cfg.settings;
    source = cfg.src;
  };
in
{

  options = {
    services.django.${app} = {
      settings = lib.mkOption {
        type = with lib.types; attrsOf anything;
        default = {};
        description = ''
          Configuration for django ${app}
        '';
      };
      src = lib.mkOption {
        type = lib.types.package;
        description = lib.mdDoc "Which DokuWiki package to use.";
      };
      port = lib.mkOption {
        type = lib.types.port;
        default = 51666;
      };
      processes = lib.mkOption {
        type = lib.types.int;
        default = 2;
      };
      threads = lib.mkOption {
        type = lib.types.int;
        default = 2;
      };
    };
  };
  config = {
    systemd.services.${user} = {
      description = "${name} django service";
      wantedBy = [ "multi-user.target" ];
      after = [ "network.target" ];
      serviceConfig = {
        User = user;
      };
      script = ''
        source ${assets.envFile}
        ${assets.managePy} migrate
        ${python}/bin/gunicorn ${app}.wsgi \
            --pythonpath ${cfg.src}/${app} \
            -b 127.0.0.1:${toString cfg.port} \
            --workers=${toString cfg.processes} \
            --threads=${toString cfg.threads}
      '';
    };
    users.users."django-${app}" = {
      isSystemUser = true;
      group = "django-${app}";
    };
    users.groups."django-${app}" = {};
}
