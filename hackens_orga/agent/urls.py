"""backend URL Configuration
"""

from rest_framework import routers

from .views import AgentViewSet

router = routers.DefaultRouter()

router.register(r"agent", AgentViewSet)

app_name = "agent-backend"
urlpatterns = router.urls
