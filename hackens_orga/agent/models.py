from django.contrib.auth.models import User
from django.db import models


class Agent(models.Model):
    name = models.CharField(max_length=255)
    user = models.OneToOneField(User, on_delete=models.SET_NULL, null=True)

    def __repr__(self):
        return f"Agent_{self.name}"

    def __str__(self):
        return self.name
