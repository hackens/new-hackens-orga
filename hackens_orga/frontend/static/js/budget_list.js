document.addEventListener('DOMContentLoaded', () => {

  const $deletebudgetline = Array.prototype.slice.call(document.querySelectorAll('.delete-budgetline'), 0);

  // Add a click event on each of them
  $deletebudgetline.forEach(el => {
    el.addEventListener('click', async () => {
      // Get the target from the "data-target" attribute
      if ('lineid' in el.dataset) {
        const target = el.dataset.lineid;
        const group = el.dataset.groupid;
        const tableLine = document.getElementById(`tableline-${target}`);
        const budgetAmount = document.getElementById(`budget-amount-${group}`);
        if (confirm("Are you sure to delete this item ?")) {
          // Get csrf token
          const cookieValue = document.cookie
            .split('; ')
            .find((row) => row.startsWith('csrftoken='))
            ?.split('=')[1];
          const url = `/api/budget/budgetline/${target}/`;
          tableLine.classList.add("tr-disabled");
          await fetch(url, {
            method: 'DELETE',
            headers: {
              'X-CSRFToken': cookieValue
            }
          }).then((resp) => {
            if(resp.ok) {
              tableLine.remove();
              const url = `/api/budget/budgetgroup/${group}`;
              budgetAmount.innerHTML = "---";
              return fetch(url);
            } else {
              tableLine.classList.remove("tr-disabled");
            }
          }).then((resp) => 
            resp.json()
          ).then((data) => {
            budgetAmount.innerHTML = data.total;
          }).catch((e) => console.log(e));
        }
      }
    });
  });

});
