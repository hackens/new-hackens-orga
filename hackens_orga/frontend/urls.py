from django.urls import include, path

from .views import (BudgetGroupCreateView, BudgetGroupUpdateView,
                    BudgetLineCreateView, BudgetLineUpdateView, BudgetListView)

app_name = "frontend"
urlpatterns = [
    path("", BudgetListView.as_view(), name="budget"),
    path(
        "budget-group/create",
        BudgetGroupCreateView.as_view(),
        name="budget-group-create",
    ),
    path(
        "budget-group/<int:pk>/update",
        BudgetGroupUpdateView.as_view(),
        name="budget-group-update",
    ),
    path(
        "budget-line/create",
        BudgetLineCreateView.as_view(),
        name="budget-line-create",
    ),
    path(
        "budget-line/<int:pk>/update",
        BudgetLineUpdateView.as_view(),
        name="budget-line-update",
    ),
]
