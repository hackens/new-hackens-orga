"""
Frontend views for the app
"""

from agent.models import Agent
from budget.models import BudgetGroup, BudgetLine
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.db.models import Model
from django.urls import reverse
from django.views.generic import (CreateView, DeleteView, DetailView, ListView,
                                  TemplateView, UpdateView)


class BudgetListView(ListView):
    model = BudgetGroup
    template_name = "frontend/budget_list.html"
    ordering = "-name"


class BudgetGroupCreateView(SuccessMessageMixin, PermissionRequiredMixin, CreateView):
    model = BudgetGroup
    fields = ["name", "description"]
    template_name = "frontend/budgetgroup_create.html"
    success_message = "Le budget %(name)s a été créé avec succès"
    permission_required = "budget.add_budgetgroup"

    def get_success_url(self):
        return reverse("frontend:budget")


class BudgetGroupUpdateView(SuccessMessageMixin, PermissionRequiredMixin, UpdateView):
    model = BudgetGroup
    fields = ["name", "description"]
    template_name = "frontend/budgetgroup_create.html"
    success_message = "Le budget %(name)s a été mis à jour avec succès"
    permission_required = "budget.change_budgetgroup"

    def get_success_url(self):
        return reverse("frontend:budget")


class BudgetLineCreateView(SuccessMessageMixin, PermissionRequiredMixin, CreateView):
    model = BudgetLine
    fields = ["title", "comment", "amount", "author", "date", "group"]
    template_name = "frontend/budgetline_create.html"
    success_message = 'La dépense/recette "%(title)s" a été créée avec succès'
    permission_required = "budget.add_budgetline"

    def get_initial(self):
        if "groupid" in self.request.GET:
            try:
                gid = int(self.request.GET["groupid"])
            except ValueError:
                gid = None
            if gid is not None:
                try:
                    grp = BudgetGroup.objects.get(pk=gid)
                except Model.DoesNotExist:
                    grp = None
            else:
                grp = None
            agents = Agent.objects.filter(user=self.user)
            return {"author": agents[0] if len(agents) >= 1 else None, "group": grp}

    def get_success_url(self):
        return reverse("frontend:budget")


class BudgetLineUpdateView(SuccessMessageMixin, UpdateView, PermissionRequiredMixin):
    model = BudgetLine
    fields = ["title", "comment", "amount", "author", "date", "group"]
    template_name = "frontend/budgetline_update.html"
    success_message = 'La dépense/recette "%(title)s" a été mise à jour avec succès'
    permission_required = "budget.change_budgetline"

    def get_success_url(self):
        return reverse("frontend:budget")
