from datetime import date

from agent.models import Agent
from django.db import models


class BudgetLine(models.Model):
    amount = models.DecimalField(
        max_digits=12, decimal_places=2, verbose_name="Montant"
    )
    author = models.ForeignKey(Agent, on_delete=models.PROTECT, verbose_name="Auteur")
    comment = models.TextField(blank=True, verbose_name="Commentaire")
    date = models.DateField(default=date.today, verbose_name="Date")
    group = models.ForeignKey(
        "BudgetGroup", on_delete=models.CASCADE, verbose_name="Budget"
    )
    title = models.CharField(max_length=255, verbose_name="Libellé")

    def __str__(self):
        return f"{self.title}"

    def __repr__(self):
        return f"BudgetLine_{self.title}_{self.amount}€"


class BudgetGroup(models.Model):
    name = models.CharField(max_length=255, verbose_name="Intitulé")
    description = models.TextField(blank=True, verbose_name="Description")

    def get_all_lines(self):
        lines = self.budgetline_set.all()
        return lines, sum(i.amount for i in lines)

    def __str__(self):
        return f"{self.name}"

    def __repr__(self):
        return f"BudgetGroup_{self.name}"
