from django.contrib import admin

from .models import BudgetGroup, BudgetLine

admin.site.register(BudgetGroup)
admin.site.register(BudgetLine)
