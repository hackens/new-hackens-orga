from rest_framework import viewsets
from rest_framework.permissions import DjangoModelPermissionsOrAnonReadOnly

from .models import BudgetGroup, BudgetLine
from .serializers import BudgetGroupSerializer, BudgetLineSerializer


class BudgetGroupViewSet(viewsets.ModelViewSet):
    queryset = BudgetGroup.objects.all()
    serializer_class = BudgetGroupSerializer
    permission_class = [DjangoModelPermissionsOrAnonReadOnly]


class BudgetLineViewSet(viewsets.ModelViewSet):
    queryset = BudgetLine.objects.all()
    serializer_class = BudgetLineSerializer
    permission_class = [DjangoModelPermissionsOrAnonReadOnly]
