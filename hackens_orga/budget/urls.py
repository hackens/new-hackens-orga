"""backend URL Configuration
"""

from rest_framework import routers

from .views import BudgetGroupViewSet, BudgetLineViewSet

router = routers.DefaultRouter()

router.register(r"budgetgroup", BudgetGroupViewSet)
router.register(r"budgetline", BudgetLineViewSet)

app_name = "budget-backend"
urlpatterns = router.urls
