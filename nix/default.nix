# nix/default.nix
{ pkgs ? import <nixpkgs> {} }:
let
  sources = import ./sources.nix;
in
import sources.nixpkgs { }
