{ djangorestframework, cryptography, django, fetchPypi, buildPythonPackage }:
buildPythonPackage rec {
  pname = "djangorestframework-knox";
  version = "4.2.0";

  src = fetchPypi {
    pname = "django-rest-knox";
    inherit version;
    sha256 = "sha256-RZXx3CPW5Br3k55fLY/a9q3gp0plYhjntWaD21Vm/Mk=";
  };

  propagatedBuildInputs = [
    django
    djangorestframework
    cryptography
  ];

  # Test raises django.core.exceptions.ImproperlyConfigured
  doCheck = false;
}
